import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LancamentoComponent } from './lancamentos.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        LancamentoComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(LancamentoComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'desafio-angular'`, () => {
    const fixture = TestBed.createComponent(LancamentoComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('desafio-angular');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(LancamentoComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('desafio-angular app is running!');
  });
});
