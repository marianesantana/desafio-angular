import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Lancamento } from '../models/lancamento';

@Injectable({
  providedIn: 'root'
})

export class LancamentoService {
  urlApi = 'https://desafio-it-server.herokuapp.com/lancamentos';

  constructor(private http: HttpClient) { }

  getAllLancamentos(): Observable<Lancamento[]> {
    return this.http.get<Lancamento[]>(this.urlApi);
  }
}