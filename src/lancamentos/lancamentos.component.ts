import { Component, OnInit } from '@angular/core';
import { LancamentoService } from './lancamentos.service';
import { Lancamento } from '../models/lancamento';

@Component({
  selector: 'app-root',
  templateUrl: './lancamentos.component.html',
  styleUrls: ['./lancamentos.component.css']
})

export class LancamentoComponent  implements OnInit{
  title = 'Desafio Angular';
  
  constructor(private lancamentoService: LancamentoService) {}

  lancamento = {} as Lancamento;
  allLancamentos: Lancamento[];
  porMes: [string, string][];

  ngOnInit(){
    this.getAllLancamentos();
    this.getLancamentos();
  }

    getLancamentos() {
      return this.lancamentoService.getAllLancamentos().subscribe((lancamentos: Lancamento[]) => {
       this.porMes = Object.entries(lancamentos.reduce((acc, lancamento) => {
          if (!acc[lancamento.mes_lancamento]){
            acc[lancamento.mes_lancamento] = 0;
          }
          acc[lancamento.mes_lancamento] = acc[lancamento.mes_lancamento] + lancamento.valor;
          console.log(acc[lancamento.mes_lancamento]);
          return acc;
        }, {}));
      });
    }

  getAllLancamentos(){
    return this.lancamentoService.getAllLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.allLancamentos = lancamentos;
      return lancamentos;
    });
  }

}




