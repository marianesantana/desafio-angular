import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './lancamentos-routing.module';
import { LancamentoComponent } from './lancamentos.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { LancamentoService } from './lancamentos.service';

@NgModule({
  declarations: [
    LancamentoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [ LancamentoService ],
  bootstrap: [LancamentoComponent]
})
export class AppModule { }
