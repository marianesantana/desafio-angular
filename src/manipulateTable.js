function somaFatura(){
    var tableFatura = document.getElementById('tb_fatura'), sumValue = {};
    for(var i = 1; i < tableFatura.rows.length; i++){
        
        sumValue[tableFatura.rows[i].cells[2].innerHTML] = (sumValue[tableFatura.rows[i].cells[2].innerHTML]? sumValue[tableFatura.rows[i].cells[2].innerHTML] : 0) + parseFloat(tableFatura.rows[i].cells[1].innerHTML.replace('R$ ','').replace('.','').replace(/,/g,'.')); 
        console.log("passei aqui")
    }
    return sumValue;
}

function drawTable(){
    
    var perMonth = somaFatura();
    var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

    var tb_somaFatura = document.createElement("table");
    tb_somaFatura.className = "table";
    tb_somaFatura.id = "tb_somaFatura";

    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    var th = document.createElement("th");
    th.scope = "col";
    th.textContent = "mês";
    tr.appendChild(th);
    th.textContent = "Total Gasto";
    tr.appendChild(th);
    thead.appendChild(tr);
    tb_somaFatura.appendChild(thead);

    var tbody = document.createElement("tbody");
    for(var i in perMonth){
        tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerText = months[i-1];
        tr.appendChild(td);
        td = document.createElement("td");
        td.innerText ="R$ "+ perMonth[i].toFixed(2).toString().replace('.',',');
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
   
    tb_somaFatura.appendChild(tbody);
    document.body.appendChild(tb_somaFatura);

}
drawTable()